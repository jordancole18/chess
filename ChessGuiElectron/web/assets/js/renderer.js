const { ipcRenderer } = require('electron');

if(document.getElementById('customdropdown') != null){
    var userInfo = ipcRenderer.sendSync('userinfo');
    document.getElementById('username').appendChild(document.createTextNode(userInfo.username));
}

if( document.getElementById('closeBtn') != null){
    document.getElementById('closeBtn').addEventListener('click', function(e){
        let Data = {
            exitcode:"1.0"
        }
        ipcRenderer.send('closeApp', Data);
    });
}

if(document.getElementById('minimizeBtn') != null){
    document.getElementById('minimizeBtn').addEventListener('click', function(e){
        let Data = {
        }
        ipcRenderer.send('minimizeApp', Data);
        console.log('here');
    });
}

if(document.getElementById('login-form') != null){
    document.getElementById('login-form').onsubmit = function(){
        let Data = {
            username: document.getElementById('loginEmail').value,
            password: document.getElementById('loginPass').value,
            rememberPass: document.getElementById('formCheck-1').value
        }
        ipcRenderer.send('loginSubmit', Data);
    }
}

function logout(){
    ipcRenderer.send('logout');
    window.location = "login.html";
}

ipcRenderer.on('goto', function(event, arg){
    window.location = arg.url;
});