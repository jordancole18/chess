const { ipcRenderer } = require('electron');

document.getElementById('closeBtn').addEventListener('click', function(e){
    let Data = {
        exitcode:"1.0"
    }
    ipcRenderer.send('closeApp', Data)
});

document.getElementById('minimizeBtn').addEventListener('click', function(e){
    let Data = {
    }
    ipcRenderer.send('minimizeApp', Data)
});

document.getElementById('login-form').onsubmit = function(){
    let Data = {
        username: document.getElementById('loginEmail').value,
        password: document.getElementById('loginPass').value
    }
    ipcRenderer.send('loginSubmit', Data)
}