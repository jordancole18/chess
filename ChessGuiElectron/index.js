const electron = require('electron');
const auth = require('./Authentication');
const url = require('url');
const path = require('path');
const __imgdir = __dirname + "/web/images/"

const {
  app, BrowserWindow, Menu, ipcMain, remote
} = electron;

let mainWindow;
let userName;
let authObj = new auth(this);

app.on('ready', function () {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    resizable: false,
    icon: __dirname + '/web/favicon.ico',
    frame: false
  });
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, "/web/login.html"),
    protocol: 'file:',
    slashes: true
  }));

  if (process.env.NODE_ENV !== 'production') {
    mainWindow.setMenu(Menu.buildFromTemplate(mainMenuTemplate));
  } else {
    mainWindow.setMenu(null);
  }

  mainWindow.setAppDetails({
    relaunchDisplayName: "Chess",
    appIconPath: __dirname + '/web/favicon.ico',
  });
});

ipcMain.on('closeApp', function(event, arg){
  app.exit(arg.exitcode);
});

ipcMain.on('minimizeApp', function(event, arg){
  BrowserWindow.getAllWindows().forEach(function(e){
    e.minimize();
  });
});

ipcMain.on('userinfo', (event, arg) =>{

  let Data = {
    username: "COOLDUDE"
  }
  event.returnValue = Data;
});

ipcMain.on('loginSubmit', (event, arg) => {
  console.log("USERNAME: " + arg.username);
  console.log("PASSWORD: " + arg.password);
  console.log("REMEMBER PASS: " + arg.rememberPass);
  userName = arg.username;
  let Data = {
    url: "dashboard.html"
  };
  //do authentication here and set auth accordingly.
  var auth = false;
  if(arg.username == '' || arg.password == ''){
    auth = false;
  }else{
    auth = authObj.auth(arg.username, arg.password);
  }
  if(!auth){
    //redirects the user back to the login page if authentication has failed.
    Data.url = "login.html";
  }
  //sends the user to the url.
  event.sender.send('goto', Data);
  event.sender.send('userinfo')
  console.log("SENDING TO " + Data.url);
});

const mainMenuTemplate = [{

    label: 'File',
    submenu: [{
      label: "Dev Tools",
      click() {
        mainWindow.toggleDevTools();
      },
      accelerator: process.platform == 'darwin' ? 'Command+Shift+I' : 'Ctrl+Shift+I'
    }]

  }

]
