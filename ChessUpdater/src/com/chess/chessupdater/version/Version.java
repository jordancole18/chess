package com.chess.chessupdater.version;

/**
 * 
 * Version.class allows you to compare two version strings
 * ex. 1.0.0 compared to 1.0.1
 * 
 * @version 1.0.0
 *
 */
public class Version implements Comparable<Version> {

	private String version;

	public final String get() {
		return this.version;
	}

	/**
	 * 
	 * @param version
	 * 
	 */
	public Version(String version) {
		if (version == null)
			throw new IllegalArgumentException("Version can not be null");
		if (!version.matches("[0-9]+(\\.[0-9]+)*"))
			throw new IllegalArgumentException("Invalid version format");
		this.version = version;
	}

	/**
	 * 
	 * @see     java.lang.Comparable#compareTo(java.lang.Object)
	 * 
	 * @return -1 if this version is less than that.
	 * @return  0 if the version is the same.
	 * @return  1 if this version if greater than that.
	 * 
	 */
	@Override
	public int compareTo(Version that) {
		if (that == null)
			return 1;
		String[] thisParts = this.get().split("\\.");
		String[] thatParts = that.get().split("\\.");
		int length = Math.max(thisParts.length, thatParts.length);
		for (int i = 0; i < length; i++) {
			int thisPart = i < thisParts.length ? Integer.parseInt(thisParts[i]) : 0;
			int thatPart = i < thatParts.length ? Integer.parseInt(thatParts[i]) : 0;
			if (thisPart < thatPart)
				return -1;
			if (thisPart > thatPart)
				return 1;
		}
		return 0;
	}

	
	@Override
	public boolean equals(Object that) {
		if (this == that)
			return true;
		if (that == null)
			return false;
		if (this.getClass() != that.getClass())
			return false;
		return this.compareTo((Version) that) == 0;
	}

}
