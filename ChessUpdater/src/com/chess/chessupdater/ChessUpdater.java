package com.chess.chessupdater;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.json.JSONObject;

import com.chess.chessupdater.update.Update;
import com.chess.chessupdater.util.Utils;
import com.chess.chessupdater.version.Version;

/**
 * 
 * @author Jordan Cole
 * @version 1.0
 *
 */
public abstract class ChessUpdater {

	/**
	 * 
	 * <p>This is an example of how this API would function.</p>
	 * 
	 * @param args
	 * 
	 */
	public static void main(String[] args) {
		ChessUpdater cu = new ChessUpdater() {

			@Override
			public void openProgram() {
				try {
					Desktop.getDesktop().open(new File(getApplicationDirectory(), "test.txt"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void closeProgram() {
				System.out.println("CLOSING APPLICATION FOR UPDATING.");
			}

			@Override
			public File getApplicationDirectory() {
				return new File("C:\\Users\\jorda\\Desktop\\Test2");
			}

		};
		cu.setVersion(new Version("0.9.9"));
		cu.setURL("https://pastebin.com/raw/DwVFGSnk");
		cu.start();
	}

	// current version of the program
	private Version version;
	// latest version of the application
	private Version latestVersion;
	// latest download url of the application
	private String latestDownloadURL;
	// update runnable which does the checking of updates
	private Update updateRunnable;
	// update thread which is the thread running updateRunnable
	private Thread updateThread;
	// the url to read from
	private String URL;

	/**
	 * 
	 * Default constructor for initialization
	 * 
	 */
	public ChessUpdater() {

	}

	/**
	 * 
	 * @see com.chess.chessupdater.update.Update
	 * 
	 *      <p>
	 *      Starts an Update thread to check for updates.
	 *      </p>
	 * 
	 */
	public void start() {
		JSONObject obj = new JSONObject(Utils.readWebpage(URL));
		latestVersion = new Version(obj.getString("latestVersion"));
		latestDownloadURL = obj.getString("latestDownloadURL");
		if (updateThread != null && updateThread.isAlive()) {
			try {
				throw new Exception("Thread has already been started.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		updateRunnable = new Update(this);
		updateThread = new Thread(updateRunnable);
		updateThread.start();
	}

	/**
	 * 
	 * Interrupts the updateThread
	 * 
	 */
	public void stop() {
		updateThread.interrupt();
	}

	/**
	 * 
	 * @return Current version
	 * 
	 */
	public Version getVersion() {
		return version;
	}

	/**
	 * 
	 * Set the current application version.
	 * 
	 * @param version
	 * 
	 */
	public void setVersion(Version version) {
		this.version = version;
	}

	/**
	 * 
	 * @return The latest version
	 * 
	 */
	public Version getLatestVersion() {
		return latestVersion;
	}

	/**
	 * 
	 * Set the latest version
	 * 
	 * @param latestVersion
	 * 
	 */
	public void setLatestVersion(Version latestVersion) {
		this.latestVersion = latestVersion;
	}

	/**
	 * 
	 * @return The latest download URL
	 * 
	 */
	public String getLatestDownloadURL() {
		return latestDownloadURL;
	}

	/**
	 * 
	 * <p>
	 * Set the latest download URL
	 * </p>
	 * 
	 * @param latestDownloadURL
	 * 
	 */
	public void setLatestDownloadURL(String latestDownloadURL) {
		this.latestDownloadURL = latestDownloadURL;
	}

	/**
	 * 
	 * Set the url to get application version data
	 * 
	 * @param URL
	 * 
	 */
	public void setURL(String URL) {
		this.URL = URL;
	}
	
	/**
	 * 
	 * @return the url to get application version data
	 * 
	 */
	public String getURL() {
		return URL;
	}
	
	/**
	 * 
	 * <p>
	 * Abstract method for other programs to state how to open their application
	 * back up
	 * </p>
	 * 
	 */
	public abstract void openProgram();

	/**
	 * 
	 * <p>
	 * Abstract method for other programs to state how to close their application
	 * for proper exit
	 * </p>
	 * 
	 */
	public abstract void closeProgram();

	/**
	 * 
	 * @return Abstract method for other programs to give the application directory
	 * 
	 */
	public abstract File getApplicationDirectory();

}
