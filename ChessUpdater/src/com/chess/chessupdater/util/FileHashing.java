package com.chess.chessupdater.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.SequenceInputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Jordan Cole
 * Oct 17, 2018
 * Github Link
 * https://github.com/jordancole18
 */

public class FileHashing {
	
	/**
	 * This is the current FileHashType
	 */
	private static FileHashType CURRENT = FileHashType.MD5;

	/**
	 * Returns the hashed checksum of a directory based on the FileHashType.
	 *
	 * @param dirToHash
	 *            The directory to hash.
	 * @param includeHiddenFiles
	 *            If you want hidden files to be included.
	 * @throws RuntimeException
	 *             If the method was unable to create a valid hashed checksum.
	 */
	public static String hashDirectory(File dirToHash, boolean includeHiddenFiles) {
		assert (dirToHash.isDirectory());
		Vector<FileInputStream> fileStreams = new Vector<FileInputStream>();

		collectInputStreams(dirToHash, fileStreams, includeHiddenFiles);

		SequenceInputStream seqStream = new SequenceInputStream(fileStreams.elements());

		try {

			String hashedString = "";

			switch (CURRENT) {
			case MD2:
				hashedString = DigestUtils.md2Hex(seqStream);
				break;
			case MD5:
				hashedString = DigestUtils.md5Hex(seqStream);
				break;
			case SHA256:
				hashedString = DigestUtils.sha256Hex(seqStream);
				break;
			case SHA384:
				hashedString = DigestUtils.sha384Hex(seqStream);
				break;
			case SHA512:
				hashedString = DigestUtils.sha512Hex(seqStream);
				break;
			default:
				hashedString = DigestUtils.md5Hex(seqStream);
				break;
			}
			seqStream.close();
			return hashedString;
		} catch (IOException e) {
			throw new RuntimeException("Error reading files to hash in " + dirToHash.getAbsolutePath(), e);
		}
	}

	/**
	 * Returns the hash checksum of a file based on the FileHashType.
	 *
	 * @param fileToHash
	 *            The file to be hashed.
	 * 
	 * @throws RuntimeException
	 *             If the method was unable to create a valid hashed checksum.
	 */
	public static String hashFile(File fileToHash) {
		assert (fileToHash.isFile());
		try {
			FileInputStream seqStream = new FileInputStream(fileToHash);
			String hashedString = "";

			switch (CURRENT) {
			case MD2:
				hashedString = DigestUtils.md2Hex(seqStream);
				break;
			case MD5:
				hashedString = DigestUtils.md5Hex(seqStream);
				break;
			case SHA256:
				hashedString = DigestUtils.sha256Hex(seqStream);
				break;
			case SHA384:
				hashedString = DigestUtils.sha384Hex(seqStream);
				break;
			case SHA512:
				hashedString = DigestUtils.sha512Hex(seqStream);
				break;
			default:
				hashedString = DigestUtils.md5Hex(seqStream);
				break;
			}
			seqStream.close();
			return hashedString;
		} catch (Exception e) {
			throw new RuntimeException("Error reading file to hash in " + fileToHash.getAbsolutePath(), e);
		}
	}

	/**
	 * 
	 * Sets the hashing algorithm.
	 * 
	 * @param hashType
	 *            The FileHashType to be set.
	 */
	public static void setFileHashType(FileHashType hashType) {
		CURRENT = hashType;
	}

	/**
	 * 
	 * @return Returns the FileHashType
	 * 
	 */
	public static FileHashType getFileHashType() {
		return CURRENT;
	}

	/**
	 * 
	 * Collects all files in a directory and puts them in a list of
	 * FileInputStream.
	 * 
	 * @param dir
	 *            The directory to search for files.
	 * @param foundStreams
	 *            The list of all found file streams in dir.
	 * @param includeHiddenFiles
	 *            Include hidden files when searching for file streams.
	 */
	private static void collectInputStreams(File dir, List<FileInputStream> foundStreams, boolean includeHiddenFiles) {

		File[] fileList = dir.listFiles();
		Arrays.sort(fileList, new Comparator<File>() {
			public int compare(File f1, File f2) {
				return f1.getName().compareTo(f2.getName());
			}
		});

		for (File f : fileList) {
			if (!includeHiddenFiles && f.getName().startsWith(".")) {
				// Skip it
			} else if (f.isDirectory()) {
				collectInputStreams(f, foundStreams, includeHiddenFiles);
			} else {
				try {
					foundStreams.add(new FileInputStream(f));
				} catch (FileNotFoundException e) {
					throw new AssertionError(e.getMessage() + ": file should never not be found!");
				}
			}
		}

	}

}

enum FileHashType {

	MD2, MD5, SHA256, SHA384, SHA512;

}