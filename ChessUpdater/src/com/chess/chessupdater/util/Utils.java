package com.chess.chessupdater.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.ProgressMonitor;
import javax.swing.ProgressMonitorInputStream;

/**
 * 
 * @author Jordan Cole
 * @version 1.0.0
 *
 */
public class Utils {

	/**
	 * 
	 * @param urlString
	 * @return the contents of the webpage
	 */
	public static String readWebpage(String urlString) {
		try {
			URL url = new URL(urlString);
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
			return sb.toString();
		} catch (MalformedURLException e) {
			System.out.println("Malformed URL: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("I/O Error: " + e.getMessage());
		}
		return null;
	}

	/**
	 * 
	 * <p>
	 * Download a file online with a progress bar.
	 * </p>
	 * 
	 * @param path
	 * @param dir
	 */
	public static File downloadFile(String urlPath, File dir) {
		assert (dir.isDirectory());
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			// Create url connection
			URL url = new URL(urlPath);
			URLConnection connection = url.openConnection();
			connection.connect();

			// Read parameters
			long length = connection.getContentLength();
			String file = "temp.zip";

			// Create streams
			ProgressMonitorInputStream pmis = new ProgressMonitorInputStream(null, "Downloading Update",
					connection.getInputStream());

			bis = new BufferedInputStream(pmis);
			bos = new BufferedOutputStream(new FileOutputStream(new File(dir, file)));

			// Setup ProgressMonitor!
			ProgressMonitor monitor = pmis.getProgressMonitor();
			monitor.setMinimum(0);
			monitor.setMaximum((int) length); // For files < 2GB
			
			// Transfer them bytes
			long read = 0L;
			while (read++ < length) {
				bos.write(bis.read());
			}
			return new File(dir, file);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bis.close();
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 
	 * <p>Unzips the contents of zipped file.</p>
	 * 
	 * @author https://www.journaldev.com/960/java-unzip-file-example
	 * 
	 * @param zipFilePath
	 * @param destDir
	 */
	public static void unzip(String zipFilePath, String destDir) {
		File dir = new File(destDir);
		// create output directory if it doesn't exist
		if (!dir.exists())
			dir.mkdirs();
		FileInputStream fis;
		// buffer for read and write data to file
		byte[] buffer = new byte[1024];
		try {
			fis = new FileInputStream(zipFilePath);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				File newFile = new File(destDir + File.separator + fileName);
				// create directories for sub directories in zip
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				// close this ZipEntry
				zis.closeEntry();
				ze = zis.getNextEntry();
			}
			// close last ZipEntry
			zis.closeEntry();
			zis.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
