package com.chess.chessupdater.update;

import java.io.File;

import org.json.JSONObject;

import com.chess.chessupdater.ChessUpdater;
import com.chess.chessupdater.util.Utils;
import com.chess.chessupdater.version.Version;

/**
 * 
 * @author Jordan Cole
 * @version 1.0.0
 *
 */
public class Update implements Runnable {
	
	private ChessUpdater chessUpdater;
	private UpdateState updateState;

	/**
	 * 
	 * Default constructor for initilization
	 * 
	 * @param chessUpdater
	 * 
	 */
	public Update(ChessUpdater chessUpdater) {
		this.chessUpdater = chessUpdater;
		updateState = UpdateState.PAUSED;
	}

	/**
	 * 
	 * @see Runnable#run()
	 * 
	 *      <p>
	 *      The run method for the runnable which checks for updates.
	 *      </p>
	 * 
	 */
	@Override
	public void run() {
		while (true) {
			if(updateState == UpdateState.UPDATING) continue;
			JSONObject obj = new JSONObject(Utils.readWebpage(chessUpdater.getURL()));
			Version latestVersion = new Version(obj.getString("latestVersion"));
			String latestURL = obj.getString("latestDownloadURL");
			//System.out.println("URL: " + latestURL);

			int num = latestVersion.compareTo(chessUpdater.getVersion());

			if (num == 1) {
				updateState = UpdateState.UPDATING;
				System.out.println("UPDATE FOUND: v" + latestVersion.get());
				
				chessUpdater.closeProgram();
				
				Runnable updatethread = new Runnable() {
					public void run() {
						File downloadedFile = Utils.downloadFile(latestURL, chessUpdater.getApplicationDirectory());
						System.out.println("FINISHED DOWNLOADING " + downloadedFile.getAbsolutePath());
						System.out.println("INSTALLING UPDATE...");
						Utils.unzip(downloadedFile.getAbsolutePath(), chessUpdater.getApplicationDirectory().getAbsolutePath());
						downloadedFile.delete();
						chessUpdater.openProgram();
						System.exit(0);
					}
				};
				new Thread(updatethread).start();
			}

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * @return An instance of ChessUpdater
	 * 
	 */
	public ChessUpdater getChessUpdater() {
		return chessUpdater;
	}

	/**
	 * 
	 * @return the current UpdateState
	 * 
	 */
	
	public UpdateState getUpdateState() {
		return updateState;
	}
	
}
enum UpdateState{
	
	PAUSED,
	UPDATING,
	COMPLETED;
	
}
