package com.chess.chessclient;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import com.chess.chessclient.manager.ClientManager;
import com.chess.chessclient.net.Client;
import com.chess.chessupdater.ChessUpdater;
import com.chess.chessupdater.version.Version;

public class ChessClient {

	// Current version of the application.
	public static String VERSION = "0.0.1";

	/**
	 * 
	 * The main start method that sets up the servers and updater.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("STARTING CLIENT ON VERSION " + VERSION);
		ChessClient chessClient = new ChessClient();
		ClientManager clientManager = new ClientManager(chessClient);
		clientManager.getChessClient();
		ChessUpdater cu = new ChessUpdater() {

			@Override
			public void closeProgram() {
				chessClient.stopClient();
				System.out.println("CLOSING CHESSCLIENT");
			}

			@Override
			public File getApplicationDirectory() {
				try {
					return new File(ChessClient.class.getProtectionDomain().getCodeSource().getLocation().toURI())
							.getParentFile();
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}

			@Override
			public void openProgram() {
				try {
					Desktop.getDesktop().open(new File(getApplicationDirectory(), "chess_updater.exe"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};
		cu.setURL("https://pastebin.com/raw/DwVFGSnk");
		cu.setVersion(new Version(VERSION));
		cu.start();
	}

	// Client instance
	private Client client;

	/**
	 * 
	 * @see com.chess.chessclient.net.Client#start();
	 * @see com.chess.chessclient.net.Client#stop();
	 * 
	 * Default ChessClient constructor
	 * 
	 */
	public ChessClient() {
		client = new Client();
		client.start();

	}

	/**
	 * 
	 * Stops the client.
	 * 
	 */
	public void stopClient() {
		client.stop();
	}

}
