package com.chess.chessclient.web;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;

import com.chess.chessclient.manager.ClientManager;

public class WebServer {

	// this allows connections to be accepted
	private boolean running = false;
	// this is the port for the web server
	private final int port;
	// this is the current url to be connected to
	private String url;
	// this is the client manager
	private ClientManager cm;

	/**
	 * The default constructor for initialization.
	 * 
	 * @param port
	 * 	
	 */
	public WebServer(int port, ClientManager cm) {
		this.running = true;
		this.port = port;
		this.url = "";
		this.cm = cm;
	}

	/**
	 * 
	 * Starts the web server and allows connections to be made.
	 * 
	 */
	public void startJavaWebServer() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					ServerSocket serverConnect = new ServerSocket(port);
					System.out.println("Web Server started.\nListening for connections on port : " + port + " ...\n");

					while (running) {
						JavaWebServer myServer = new JavaWebServer(serverConnect.accept(), port, cm);
						myServer.setCurrentURL(url);
						Thread thread = new Thread(myServer);
						thread.start();
					}
					serverConnect.close();
				} catch (IOException e) {
					System.err.println("Server Connection error : " + e.getMessage());
					if (e instanceof BindException) {
						System.err.println("Try changing your website port.");
						System.exit(-1);
					}
				}
			}

		}).start();
	}

	/**
	 * 
	 * @return if the server is accepting connections.
	 * 
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * 
	 * Stops connections from being made to the server.
	 * 
	 */
	public void stop() {
		System.out.println("Stopping web server...");
		running = false;
	}

	/**
	 * 
	 * @return the current port the server is running on.
	 * 
	 */
	public int getPort() {
		return port;
	}
}
