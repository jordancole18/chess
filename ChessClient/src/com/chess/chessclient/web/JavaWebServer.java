package com.chess.chessclient.web;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.StringTokenizer;

import com.chess.chessclient.manager.ClientManager;

/**
 * 
 * @author Jordan Cole
 * @version v1.0.0
 *
 */
public class JavaWebServer implements Runnable {

	//the current port
	private final int PORT;
	//the url to redirect to
	private String URL;
	//the connection
	private Socket connect;
	//the root web file
	private File WEB_ROOT;
	//the name of the default web file
	private String DEFAULT_FILE = "index.php";
	//the method not support web file
	private String METHOD_NOT_SUPPORTED = "not_supported.html";
	//the file not found web file
	private String FILE_NOT_FOUND = "404.html";
	//client manager instance
	private ClientManager cm;

	/**
	 * 
	 * Default constructor used for initialization.
	 * 
	 * @param connection
	 * @param PORT
	 * @param ws
	 */
	public JavaWebServer(Socket c, int PORT, ClientManager cm) {
		connect = c;
		this.PORT = PORT;
		this.URL = "";
		WEB_ROOT = cm.getWebFolder();
		this.cm = cm;
	}

	/**
	 * 
	 * The connection is initialized and the html is sent over to the client.
	 * 
	 */
	@Override
	public void run() {
		BufferedReader in = null;
		PrintWriter out = null;
		BufferedOutputStream dataOut = null;
		String fileRequested = null;

		try {
			in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
			out = new PrintWriter(connect.getOutputStream());
			dataOut = new BufferedOutputStream(connect.getOutputStream());
			String input = in.readLine();
			StringTokenizer parse = null;
			try {
				parse = new StringTokenizer(input);
			} catch (NullPointerException e) {
				in.close();
				out.close();
				dataOut.close();
				connect.close();
				return;
			}

			String method = parse.nextToken().toUpperCase();

			fileRequested = parse.nextToken().toLowerCase();

			System.out.println("TRYING TO GET: " + fileRequested);

			if (!method.equals("GET") && !method.equals("HEAD")) {
				File file = new File(WEB_ROOT, METHOD_NOT_SUPPORTED);
				int fileLength = (int) file.length();
				String contentMimeType = "text/html";

				byte[] fileData = readFileData(file, fileLength);

				out.println("HTTP/1.1 501 Not Implemented");
				out.println("Server: Java HTTP Server");
				out.println("Date: " + new Date());
				out.println("Content-type: " + contentMimeType);
				out.println("Content-length: " + fileLength);
				out.println();
				out.flush();
				dataOut.write(fileData, 0, fileLength);
				dataOut.flush();

			} else {
				if (fileRequested.endsWith("/")) {
					fileRequested += DEFAULT_FILE;
				}

				File file = new File(WEB_ROOT, fileRequested);
				
				if(!file.exists()) {
					fileRequested = "/index.html";
					file = new File(WEB_ROOT, fileRequested);
				}
				
				int fileLength = (int) file.length();
				String content = getContentType(fileRequested);

				if (method.equals("GET")) {
					byte[] fileData = null;
					int newLength = 0;

					if (fileRequested.endsWith(".php")) {

						String exec = execPHP(fileRequested, "");
						// System.out.println("PHP: " + exec);
						
						String html = "<html><body data-gr-c-s-loaded=\"true\">" + exec + "</body></html>";

						newLength = html.length();
						fileData = html.getBytes();
					} else {
						newLength = fileLength;
						fileData = readFileData(file, fileLength);
					}

					out.println("HTTP/1.1 200 OK");
					out.println("Server: Java HTTP Server");
					out.println("Date: " + new Date());
					out.println("Content-type: " + content);
					out.println("Content-length: " + newLength);
					out.println();
					out.flush();

					dataOut.write(fileData, 0, newLength);
					dataOut.flush();
				}

			}

		} catch (FileNotFoundException fnfe) {
			try {
				fileNotFound(out, dataOut, fileRequested);
			} catch (IOException ioe) {
				System.err.println("Error with file not found exception : " + ioe.getMessage());
			}

		} catch (IOException ioe) {
			System.err.println("Server error : " + ioe);
		} finally {
			try {
				in.close();
				out.close();
				dataOut.close();
				connect.close();
			} catch (Exception e) {
				System.err.println("Error closing stream : " + e.getMessage());
			}
		}
	}

	/**
	 * 
	 * @param scriptName
	 * @param param
	 * @return executed PHP output.
	 */
	public String execPHP(String scriptName, String param) {
		StringBuilder output = new StringBuilder();
		try {
			String line;
			Process p = Runtime.getRuntime().exec("\"" + new File(cm.getPhpFolder(), "/php.exe").getAbsolutePath() + "\" \""
					+ new File(WEB_ROOT, scriptName.substring(1)).getAbsolutePath() + "\" \"" + param + "\"");
			System.out.println("\"" + new File(cm.getPhpFolder(), "/php.exe").getAbsolutePath() + "\" \""
					+ new File(WEB_ROOT, scriptName.substring(1)).getAbsolutePath() + "\" \"" + param + "\"");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				output.append(line);
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		return output.toString();
	}

	/**
	 * 
	 * Reads a file and returns a byte array of file contents.
	 * 
	 * @param file
	 * @param fileLength
	 * @return byte[]
	 * @throws IOException
	 * 
	 */
	private byte[] readFileData(File file, int fileLength) throws IOException {
		FileInputStream fileIn = null;
		byte[] fileData = new byte[fileLength];

		try {
			fileIn = new FileInputStream(file);
			fileIn.read(fileData);
		} finally {
			if (fileIn != null)
				fileIn.close();
		}

		return fileData;
	}

	/**
	 * 
	 * @param fileRequested
	 * @return supported MIME Types
	 */
	private String getContentType(String fileRequested) {
		if (fileRequested.endsWith(".htm") || fileRequested.endsWith(".html") || fileRequested.endsWith(".php"))
			return "text/html";
		else
			return "text/plain";
	}

	/**
	 * 
	 * Shows a 404 page when fileRequest can not be found.
	 * 
	 * @param out
	 * @param dataOut
	 * @param fileRequested
	 * @throws IOException
	 */
	private void fileNotFound(PrintWriter out, OutputStream dataOut, String fileRequested) throws IOException {
		File file = new File(WEB_ROOT, FILE_NOT_FOUND);
		int fileLength = (int) file.length();
		String content = "text/html";
		byte[] fileData = readFileData(file, fileLength);

		out.println("HTTP/1.1 404 File Not Found");
		out.println("Server: Java HTTP Server");
		out.println("Date: " + new Date());
		out.println("Content-type: " + content);
		out.println("Content-length: " + fileLength);
		out.println();
		out.flush();

		dataOut.write(fileData, 0, fileLength);
		dataOut.flush();
	}

	/**
	 * 
	 * @return the connection
	 * 
	 */
	public Socket getConnection() {
		return connect;
	}

	/**
	 * 
	 * @return the port the web server is running on.
	 * 
	 */
	public int getPort() {
		return PORT;
	}

	/**
	 * 
	 * @return the current url to redirect to.
	 * 
	 */
	public String getCurrentURL() {
		return URL;
	}

	/**
	 * 
	 * Sets the current url to redirect to.
	 * 
	 * @param url
	 * 
	 */
	public void setCurrentURL(String url) {
		this.URL = url;
	}

}
