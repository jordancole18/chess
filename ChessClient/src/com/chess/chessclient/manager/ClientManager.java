package com.chess.chessclient.manager;

import java.io.File;
import java.net.URISyntaxException;

import com.chess.chessclient.ChessClient;
import com.chess.chessclient.config.FileManager;
import com.chess.chessclient.web.WebServer;

import net.lingala.zip4j.core.ZipFile;

public class ClientManager {

	private WebServer webServer;
	private ChessClient chessClient;
	private FileManager fileManager;
	private File applicationFolder;
	private File webFolder;
	private File chessFolder;
	private File phpFolder;

	public ClientManager(ChessClient cc) {
		this.chessClient = cc;
		fileManager = FileManager.getInstance();
		fileManager.setup(this);
		try {
			applicationFolder = new File(ChessClient.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
			webFolder = new File(applicationFolder, "/web/");
			chessFolder = new File("C:/chess/");
			phpFolder = new File(chessFolder, "/php/");
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(applicationFolder == null) {
			try {
				throw new Exception("Invalid Application Folder.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!webFolder.exists()) {
			webFolder.mkdir();
		}
		
		if(!chessFolder.exists()) {
			chessFolder.mkdirs();
		}
		
		try {
			if(!new File(chessFolder, "/php/").exists()) {
				File phpZip = new File(chessFolder, "php.zip");
				if(phpZip.exists()) {
					phpZip.delete();
				}
				FileManager.getInstance().copyFileToDirectory("php.zip", phpZip);
				ZipFile zip = new ZipFile(phpZip);
				zip.extractAll(chessFolder.getAbsolutePath());
				phpZip.delete();
			}
			if(!new File(webFolder, "index.php").exists() && !new File(webFolder, "index.html").exists()) {
				FileManager.getInstance().copyFileToDirectory("index.php", new File(webFolder, "index.php"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		webServer = new WebServer(8080, this);
		webServer.startJavaWebServer();
	}

	public WebServer getWebServer() {
		return webServer;
	}

	public ChessClient getChessClient() {
		return chessClient;
	}

	public File getApplicationFolder() {
		return applicationFolder;
	}
	
	public File getWebFolder() {
		return webFolder;
	}
	
	public File getChessFolder() {
		return chessFolder;
	}
	
	public File getPhpFolder() {
		return phpFolder;
	}
	
}
