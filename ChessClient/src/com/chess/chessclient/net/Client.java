package com.chess.chessclient.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import com.chess.chessclient.config.FileManager;

import oshi.SystemInfo;

public class Client {
	private Socket connection;
	private String message;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private boolean stop;
	public boolean connect = false;

	public Client() {
	}

	public void start() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				connectToServer();
			}
		});
		t.start();
	}

	public void connectToServer() {
		System.out.println("ATTEMPTING SERVER CONNECTION");
		if (!FileManager.getInstance().getConfig().contains("server.ipv4")) {
			System.err.println("Applications needs to be configured! No IPV4 has been set to connect to host.");
			return;
		}
		try {
			this.connection = new Socket(FileManager.getInstance().getConfig().getString("server.ipv4"), 8222);
			this.setupStreams();
			this.whileChatting();
			this.connect = true;
		} catch (UnknownHostException var8) {
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException var7) {
				var7.printStackTrace();
			}

			this.connectToServer();
		} catch (IOException var9) {
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException var6) {
				var6.printStackTrace();
			}

			this.connectToServer();
		}

	}

	public void setupStreams() throws IOException {
		this.output = new ObjectOutputStream(this.connection.getOutputStream());
		this.output.flush();
		this.input = new ObjectInputStream(this.connection.getInputStream());
		System.out.println("CONNECTED TO SERVER");
		sendHostMessage("ipv4:" + getIPv4());
		new Thread(() -> {
			while (true) {
				try {
					try {
						SystemInfo si = new SystemInfo();
						long avail = si.getHardware().getMemory().getAvailable();
						double mem = (double) avail / si.getHardware().getMemory().getTotal();
						double freeMemPercent = 100 - (mem * 100);
						double cpu = si.getHardware().getProcessor().getSystemCpuLoad() * 100;

						sendHostMessage("cpuusage:" + round(cpu, 2));
						sendHostMessage("usage:" + round(freeMemPercent, 2));
					} catch (Exception e) {
						e.printStackTrace();
					}
					Thread.sleep(2000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public void whileChatting() throws IOException {
		do {
			try {
				this.message = (String) this.input.readObject();
				System.out.println(this.message);
			} catch (ClassNotFoundException var9) {

			}
		} while (!stop);

	}

	private String getIPv4() {
		String ip = null;
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface iface = interfaces.nextElement();

				if (iface.isLoopback() || !iface.isUp())
					continue;

				Enumeration<InetAddress> addresses = iface.getInetAddresses();
				while (addresses.hasMoreElements()) {
					InetAddress addr = addresses.nextElement();

					if (addr instanceof Inet6Address)
						continue;

					ip = addr.getHostAddress();
					System.out.println(iface.getDisplayName() + " " + ip);
				}
			}
		} catch (SocketException e) {
			throw new RuntimeException(e);
		}
		return ip;
	}

	public void sendHostMessage(String message) {
		try {
			this.output.writeObject(message);
			this.output.flush();
		} catch (IOException var3) {

		}

	}

	public void stop() {
		this.stop = true;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
