package com.chess.chessclient.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.simpleyaml.configuration.file.FileConfiguration;
import org.simpleyaml.configuration.file.YamlConfiguration;

import com.chess.chessclient.manager.ClientManager;

public class FileManager {

	static FileManager instance = new FileManager();

	private FileConfiguration config;
	private File cfile;

	public static FileManager getInstance() {
		return instance;
	}

	public void setup(ClientManager cm) {
		cfile = new File(cm.getApplicationFolder(), "config.yml");
		config = YamlConfiguration.loadConfiguration(cfile);

		if (!(cfile.exists())) {
			config.set("server.ipv4", "localhost");
			try {
				config.save(cfile);
			} catch (IOException e) {

			}
		}

	}

	public void copyFileToDirectory(String resourcePath, File destination) {
	    try {
	        InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(resourcePath);
	        FileUtils.copyInputStreamToFile(in, destination);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	/**
	 * 
	 * @return the config
	 * 
	 */
	public FileConfiguration getConfig() {
		return config;
	}

	/**
	 * 
	 * Save the config
	 * 
	 * @throws IOException
	 * 
	 */
	public void saveConfig() throws IOException {
		config.save(cfile);
	}

	/**
	 * 
	 * Reload the config.
	 * 
	 */
	public void reloadConfig() {
		config = YamlConfiguration.loadConfiguration(cfile);
	}

}
