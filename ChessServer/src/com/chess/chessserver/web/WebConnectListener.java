package com.chess.chessserver.web;

/**
 * 
 * @author Jordan Cole
 * @version v1.0.0
 *
 */
public interface WebConnectListener {

	/**
	 * 
	 * This method is ran when a connection is made to the web server.
	 * 
	 * @param connection
	 * 
	 */
	void onConnect(JavaWebServer connection);

}
