package com.chess.chessserver.web;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * 
 * @author Jordan Cole
 * @version v1.0.0
 *
 */
public class JavaWebServer implements Runnable {

	// the port for the web server
	private final int PORT;
	// the url to redirect to
	private String URL;
	// the connection to the server
	private Socket connect;
	// the web server handler.
	private WebServer ws;

	/**
	 * 
	 * Default constructor used for initialization.
	 * 
	 * @param connection
	 * @param PORT
	 * @param ws
	 */
	public JavaWebServer(Socket c, int PORT, WebServer ws) {
		connect = c;
		this.PORT = PORT;
		this.URL = "";
		this.ws = ws;
	}

	/**
	 * 
	 * The connection is initialized and the html is sent
	 * over to the client.
	 * 
	 */
	@Override
	public void run() {
		BufferedReader in = null;
		PrintWriter out = null;
		BufferedOutputStream dataOut = null;
		try {
			in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
			out = new PrintWriter(connect.getOutputStream());
			dataOut = new BufferedOutputStream(connect.getOutputStream());
			String input = in.readLine();
			StringTokenizer parse = new StringTokenizer(input);
			String method = parse.nextToken().toUpperCase();
			String fileRequested = parse.nextToken().toLowerCase();
			
			String contentMimeType = "text/html";

			System.out.println(method + fileRequested);
			
			if(!fileRequested.equals("/")){
				in.close();
				out.close();
				dataOut.close();
				connect.close();
				return;
			}
			
			ws.onConnect(this);

			if (URL == null || URL.equals("")) {
				System.err.println("Could not make request to the specified URL. The url is not set.");
				in.close();
				out.close();
				dataOut.close();
				connect.close();
				return;
			}

			String html = "<!DOCTYPE HTML>\r\n" + " \r\n" + "<meta charset=\"UTF-8\">\r\n"
					+ "<meta http-equiv=\"refresh\" content=\"1; url=" + URL + "\">\r\n" + " \r\n" + "<script>\r\n"
					+ "  window.location.href = \"" + URL + "\"\r\n" + "</script>\r\n";
			out.println("HTTP/1.1 200 Ok");
			out.println("Server: ChessServer");
			out.println("Date: " + new Date());
			out.println("Content-type: " + contentMimeType);
			out.println("Content-length: " + html.length());
			out.println();
			out.flush();
			dataOut.write(html.getBytes(), 0, html.length());
			dataOut.flush();

		} catch (FileNotFoundException fnfe) {
			System.out.println("ERROR FINDING WEB FILES..");
		} catch (IOException ioe) {
			System.err.println("Server error : " + ioe);
		} finally {
			try {
				in.close();
				out.close();
				dataOut.close();
				connect.close();
			} catch (Exception e) {
				System.err.println("Error closing stream : " + e.getMessage());
			}
		}
	}

	/**
	 * 
	 * @return the connection
	 * 
	 */
	public Socket getConnection() {
		return connect;
	}

	/**
	 * 
	 * @return the port the web server is running on.
	 * 
	 */
	public int getPort() {
		return PORT;
	}

	/**
	 * 
	 * @return the current url to redirect to.
	 * 
	 */
	public String getCurrentURL() {
		return URL;
	}

	/**
	 * 
	 * Sets the current url to redirect to.
	 * 
	 * @param url
	 * 
	 */
	public void setCurrentURL(String url) {
		this.URL = url;
	}

}
