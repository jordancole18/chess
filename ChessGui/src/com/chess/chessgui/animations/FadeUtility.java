package com.chess.chessgui.animations;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

/**
 * 
 * @author https://stackoverflow.com/a/18542844
 *
 */
public class FadeUtility {
	private static final int TIME = 100;
	private static final int MILLIS_PER_FRAME = 15;
	private static final float DELTA = MILLIS_PER_FRAME / (float) TIME;

	/**
	 * @param frame the frame to fade in or out
	 * @param in    true if you are fading in, false if you're fading out
	 */
	public static void fade(final JFrame frame, final boolean in) {
		float WIDTH_DELTA = 800 / MILLIS_PER_FRAME;
		float HEIGHT_DELTA = 600 / MILLIS_PER_FRAME;
		frame.setOpacity(in ? 0f : 1f);
		if (in) {
			frame.setState(JFrame.NORMAL);
		}
		final Timer timer = new Timer();
		TimerTask timerTask = new TimerTask() {
			float opacity = in ? 0f : 1f;
			float delta = in ? DELTA : -DELTA;

			@Override
			public void run() {
				opacity += delta;
				if (in) {
					frame.setSize((int) (frame.getWidth() + WIDTH_DELTA), (int) (frame.getHeight() + HEIGHT_DELTA));
					frame.setLocation((int) frame.getLocation().getX() + 10, (int) frame.getLocation().getY() - 40);
				} else {
					frame.setSize((int) (frame.getWidth() - WIDTH_DELTA), (int) (frame.getHeight() - HEIGHT_DELTA));
					frame.setLocation((int) frame.getLocation().getX() - 10, (int) frame.getLocation().getY() + 40);
				}

				if (opacity < 0) {
					frame.setState(JFrame.ICONIFIED);
					frame.setOpacity(1f);
					timer.cancel();
				} else if (opacity > 1) {
					frame.setOpacity(1f);
					timer.cancel();
				} else {
					frame.setOpacity(opacity);
				}
			}
		};
		timer.scheduleAtFixedRate(timerTask, MILLIS_PER_FRAME, MILLIS_PER_FRAME);
	}
}
