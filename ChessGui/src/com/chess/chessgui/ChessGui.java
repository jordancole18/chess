package com.chess.chessgui;

import com.chess.chessgui.frame.GuiWindow;
import com.chess.chessgui.manager.ImageManager;

public class ChessGui {

	public static void main(String[] args) {
		new ChessGui();
	}
	
	private GuiWindow mainWindow;
	private ImageManager imageManager;
	
	public ChessGui() {
		this.imageManager = new ImageManager();
		this.mainWindow = new GuiWindow("Chess", 800, 600, imageManager);
	}
	
	public GuiWindow getMainWindow() {
		return mainWindow;
	}
	
	public ImageManager getImageManager() {
		return imageManager;
	}
	
}
