package com.chess.chessgui.manager;

import java.awt.image.BufferedImage;

public class ImageManager {
	
	public BufferedImage getImage(String name, int width, int height) {
		return FileManager.resize(FileManager.getFileAsBufferedImage(name), width, height);
	}
	
}
