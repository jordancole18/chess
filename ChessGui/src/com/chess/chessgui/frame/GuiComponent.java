package com.chess.chessgui.frame;

import javax.swing.JPanel;

import com.chess.chessgui.manager.ImageManager;

public class GuiComponent extends JPanel{

	private static final long serialVersionUID = 1L;

	private GuiWindow mainWindow;
	
	public GuiComponent(GuiWindow mainWindow, int width, int height) {
		this.setSize(width, height);
		this.setLayout(null);
		this.mainWindow = mainWindow;
	}
	
	public GuiWindow getGuiWindow() {
		return mainWindow;
	}
	
	public ImageManager getImageManager() {
		return mainWindow.getImageManager();
	}
	
}
