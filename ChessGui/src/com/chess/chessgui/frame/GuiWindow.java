package com.chess.chessgui.frame;

import java.awt.Color;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import com.chess.chessgui.animations.FadeUtility;
import com.chess.chessgui.frame.components.TopBarComponent;
import com.chess.chessgui.manager.ImageManager;

public class GuiWindow extends JFrame{

	private static final long serialVersionUID = 7471910496668635593L;
	private List<GuiComponent> guiComponents;
	private ImageManager imageManager;
	private boolean minimize;
	
	public GuiWindow(String title, int width, int height, ImageManager imageManager) {
		super(title);
		this.setSize(width, height);
		this.setUndecorated(true);
		this.setLayout(null);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setBackground(new Color(125, 125, 125));
		this.guiComponents = new ArrayList<GuiComponent>();
		this.imageManager = imageManager;
		this.addComponent(new TopBarComponent(this));
		GuiWindow instance = this;
		this.addWindowStateListener(new WindowStateListener() {

			@Override
			public void windowStateChanged(WindowEvent we) {
				int state = we.getNewState();
				if(state == 0) {
					if(minimize) {
						FadeUtility.fade(instance, true);
						minimize = false;
					}
				}else if(state == 1) {
					if(!minimize) {
						FadeUtility.fade(instance, false);
						minimize = true;
					}
				}
			}
			
		});
		this.setVisible(true);
	}
	
	public void addComponent(GuiComponent guiComp) {
		guiComponents.add(guiComp);
		add(guiComp);
		repaint();
	}
	
	public ImageManager getImageManager() {
		return imageManager;
	}
	
	public void setMinimize(boolean b) {
		this.minimize = b;
	}
	
}
