package com.chess.chessgui.frame.components;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.chess.chessgui.animations.FadeUtility;
import com.chess.chessgui.frame.GuiComponent;
import com.chess.chessgui.frame.GuiWindow;

public class TopBarComponent extends GuiComponent {

	private static final long serialVersionUID = 1L;

	private Point initialClick;
	private JLabel appName;
	private JButton exitBtn;
	private JButton minimizeBtn;

	public TopBarComponent(GuiWindow mainWindow) {
		super(mainWindow, 800, 25);
		this.setLocation(0, 0);
		this.setBackground(new Color(75, 75, 75));
		this.initListeners();

		appName = new JLabel("CHESS");
		appName.setSize(50, 25);
		appName.setLocation(10, 0);
		appName.setForeground(Color.WHITE);

		exitBtn = new JButton() {
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				if (getModel().isPressed()) {
					g.setColor(new Color(100, 100, 100));
					g.fillRect(0, 0, getWidth(), getHeight());
				} else if (getModel().isRollover()) {
					g.setColor(Color.RED);
					g.fillRect(0, 0, getWidth(), getHeight());
				}
				super.paintComponent(g);
			}
		};
		exitBtn.setSize(25, 25);
		exitBtn.setContentAreaFilled(false);
		exitBtn.setFocusPainted(false);
		exitBtn.setBorderPainted(false);
		exitBtn.setIcon(new ImageIcon(getImageManager().getImage("closeBtn.png", 16, 16)));
		exitBtn.setLocation(mainWindow.getWidth() - 25, 0);
		exitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});

		minimizeBtn = new JButton() {
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				if (getModel().isPressed()) {
					g.setColor(new Color(100, 100, 100));
					g.fillRect(0, 0, getWidth(), getHeight());
				} else if (getModel().isRollover()) {
					g.setColor(Color.WHITE);
					float opacity = 0.5f;
					Graphics2D g2d = (Graphics2D) g;
					g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
					g2d.fillRect(0, 0, getWidth(), getHeight());
					g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
				}
				super.paintComponent(g);
			}
		};

		minimizeBtn.setSize(25, 25);
		minimizeBtn.setIcon(new ImageIcon(getImageManager().getImage("minimizeBtn.png", 16, 16)));
		minimizeBtn.setContentAreaFilled(false);
		minimizeBtn.setFocusPainted(false);
		minimizeBtn.setBorderPainted(false);
		minimizeBtn.setLocation(mainWindow.getWidth() - 50, 0);
		minimizeBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mainWindow.setMinimize(true);
				FadeUtility.fade(mainWindow, false);
			}

		});

		add(minimizeBtn);
		add(exitBtn);
		add(appName);
	}

	public void initListeners() {
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				initialClick = e.getPoint();
				getComponentAt(initialClick);
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (initialClick == null)
					return;
				int thisX = getGuiWindow().getLocation().x;
				int thisY = getGuiWindow().getLocation().y;
				int xMoved = e.getX() - initialClick.x;
				int yMoved = e.getY() - initialClick.y;
				int X = thisX + xMoved;
				int Y = thisY + yMoved;
				float dist = (float) Math
						.sqrt(Math.pow(X - getGuiWindow().getX(), 2) + Math.pow(Y - getGuiWindow().getY(), 2));
				//allows smooth window snapping
				if(dist > 5 && dist <= 10) {
					e.consume();
					return;
				}
				getGuiWindow().setLocation(X, Y);
			}
		});
	}

}
